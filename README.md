# How this task will be evaluated
## We expect your solution to the task to meet the following criteria:

* You have uncommented the header section in HTML and arranged it horizontally
* The section with the content contains two columns
* The footer section contains 3 or 7 columns
* You have placed the information in the footer according to the image you chose: example A or example B
* Your markup matches the specific image in the task
* Please pay attention that pixel-perfect is not required. Different designs are related to the educational purpose. Designs were created to cover different cases under markup elements position.

You can get the maximum of 60 points (100%) for this task. To pass the task, you need to get at least 70% of the points.
